/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонус анимация появления слайдов через навешивание дополнительных стилей

*/

  var ourSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
  var currentPosition = 0;


function initSlider() {
    window.addEventListener('load', function() {
        renderImage(currentPosition);
    });

    var nextBtn = document.getElementById('NextSilde');
        nextBtn.addEventListener('click', nextSlide);

    var prevBtn = document.getElementById('PrevSilde');
        prevBtn.addEventListener('click', prevSlide);
}

function renderImage(id) {

    if ( id === -1 ) {
     id = ourSliderImages.length - 1;
    } 
    else if ( ourSliderImages.length === id ) {
      id = 0;
    }
    
    currentPosition = id;

    var slider = document.getElementById('slider');

      slider.style.position = 'relative';
      slider.style.width = '512px';
      slider.style.height = '512px';


      slider.innerHTML = '';
    
        

    var image = document.createElement('img');
      
      image.src = ourSliderImages[id];

// css
      image.style.transition   = 'all .3s ease .3s';
      image.style.opacity      = '0';
      image.style.position     = 'absolute';
      image.style.top          = '50%';
      image.style.left         = '50%';
      image.style.right        = '50%';
      image.style.bottom       = '50%';
      image.style.width        = '0';
      image.style.height       = '0';


      setTimeout( function(){
        image.style.position     = 'absolute';
        image.style.top          = '0';
        image.style.right        = '0';
        image.style.bottom       = '0';
        image.style.left         = '0';
        image.style.width        = '512px';
        image.style.height       = '512px';
        image.style.opacity      = '1';
      }, 200);



      slider.appendChild(image);
}

function nextSlide() {
  renderImage(++currentPosition);
}

function prevSlide() {

  renderImage(--currentPosition);

}

initSlider();
