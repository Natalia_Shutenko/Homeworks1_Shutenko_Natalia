/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/
  let colors = {
    background: 'purple',
    color: 'white'
  }

  function addTitle () {
    
    let title = document.createElement('h1');
        wrap  = document.getElementById('wrap');

    title.innerText = 'I know how binding works in JS';
    
    wrap.appendChild(title);
  } 

  function myCall( back ){
    addTitle ();
    document.body.style.background = back;
    document.body.style.color = this.color;

  }

  function myBind () {
    addTitle ();
    document.body.style.background = this.background;
    document.body.style.color = this.color;
  }

  myCall.call( colors, 'red' );
  
  const bindedFunc = myBind.bind(colors);
  bindedFunc();

  function myApply(  ) {

    document.body.style.background = this.background;
    document.body.style.color = this.color;

    let title = document.createElement('h1');
        wrap  = document.getElementById('wrap');

    title.innerText = text;
    
    wrap.appendChild(title);
  }

var text = ['I know how apply works in JS'];

myApply.apply(colors, text);