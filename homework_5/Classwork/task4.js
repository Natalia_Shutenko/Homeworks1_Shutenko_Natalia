/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.

    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1('Sdwq', 3);
      decryptCesar1(...)
      ...
      decryptCesar5(...)

*/
function encryptCesar(key, word) {
    let encryptedWord = '';

    for (let i = 0, length = word.length; i < length; i++) {
        let letter = word.charCodeAt(i);
            letter = letter + key;
            encryptedWord += String.fromCharCode(letter);
    }

    return encryptedWord;
}


function decryptCesar(key, word) {
    let decryptedWord = '';

    for (let i = 0, length = word.length; i < length; i++) {
        let letter = word.charCodeAt(i);
            letter = letter - key;
            decryptedWord += String.fromCharCode(letter);
    }
    return decryptedWord;
}

let encryptCesar1 = encryptCesar.bind(null, 1);
let encryptCesar2 = encryptCesar.bind(null, 2);
let encryptCesar3 = encryptCesar.bind(null, 3);
let encryptCesar4 = encryptCesar.bind(null, 4);
let encryptCesar5 = encryptCesar.bind(null, 5);

let decryptCesar1 = decryptCesar.bind(null, 1);
let decryptCesar2 = decryptCesar.bind(null, 2);
let decryptCesar3 = decryptCesar.bind(null, 3);
let decryptCesar4 = decryptCesar.bind(null, 4);
let decryptCesar5 = decryptCesar.bind(null, 5);



console.log(encryptCesar1('ilikejavascript') + ' ' + decryptCesar1(encryptCesar1('ilikejavascript')));
console.log(encryptCesar2('sunshinning') + ' ' + decryptCesar2(encryptCesar2('sunshinning')));
console.log(encryptCesar3('windofwinter') + ' ' + decryptCesar3(encryptCesar3('windofwinter')));
console.log(encryptCesar4('harrypotter') + ' ' + decryptCesar4(encryptCesar4('harrypotter')));
console.log(encryptCesar5('firstofmogikan') + ' ' + decryptCesar5(encryptCesar5('firstofmogikan')));
