// /*

//   Задание:

//     1. Написать конструктор объекта комментария который принимает 3 аргумента
//     ( имя, текст сообщения, ссылка на аватаку);

//     {
//       name: '',
//       text: '',
//       url: '...jpg'
//       likes: 0
//     }
//       + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
//       + В прототипе должен быть метод который увеличивает счетик лайков

//     var myComment1 = new Comment(...);

//     2. Создать массив из 4х комментариев.
//     var CommentsArray = [myComment1, myComment2...]

//     3. Созадть функцию конструктор, которая принимает массив коментариев.
//       И выводит каждый из них на страничку.

//     <div id="CommentsBox"></div>

// */

window.addEventListener("DOMContentLoaded", function () {

    function ProtoComment() {

        this.url = 'https://i.imgur.com/TqkjHYH.png';

        this.addLike = function() {
            ++this.like;
            console.log(this.like);
            return this.like;
        }
    }


    let CommentsArray = [];

    function Comment(name, text, url) {

        Object.setPrototypeOf(this, new ProtoComment());

        this.name = name;
        this.text = text;
        this.url = url || 'https://i.imgur.com/TqkjHYH.png';

        this.addLike = this.addLike.bind(this);
        this.like = 0;

        CommentsArray.push(this);
    }

    let feed = document.getElementById('CommentsFeed');

    function CreatePost(commentsArray) {

        commentsArray.forEach(function (element) {

            let post = document.createElement('div');

            let avatar = new Image(50, 50);
            avatar.src = element.url;
            post.appendChild(avatar);

            let name = document.createElement('span');
            name.innerHTML = element.name;
            name.style.fontSize = '20px';
            name.style.fontWeight = 'bold';
            post.appendChild(name);


            let text = document.createElement('div');
            text.innerHTML = element.text;
            post.appendChild(text);

            let like = document.createElement('span');
            like.innerText = element.like;
            console.log(element.like);

            post.appendChild(like);

            let likeBtn = document.createElement('button');
            likeBtn.innerHTML = 'Like';
            post.appendChild(likeBtn);

            likeBtn.addEventListener('click', function () {
                element.addLike();
                like.innerText = element.like;
            });

            feed.appendChild(post);

        })

    }

    let comment1 = new Comment('Natali', 'comment 1', 'https://imgur.com/WkYH28D.png');
    let comment2 = new Comment('Olga', 'comment 2', 'https://imgur.com/CFDxkSs.png');
    let comment3 = new Comment('Alex', 'comment 3');
    let comment4 = new Comment('Ihor', 'comment 4', 'https://imgur.com/EBvzWcF.png');

    CreatePost(CommentsArray);


});
