/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания) +

  + Бонус, повесить обработчик на кнопку через метод onClick +
  + Бонус, использовать 6-ричную систему исчесления и цвет HEX -> #FFCC00 +
  + Бонус выводить полученый цвет по центру страницы.
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 256;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    indexColor = Math.floor(Math.random() * (max - min + 1)) + min;
    return indexColor;
  }


getRandomIntInclusive(0, 255);

document.body.style.background = 'rgba('+ getRandomIntInclusive(0, 255) +', '+ getRandomIntInclusive(0, 255) +', '+ getRandomIntInclusive(0, 255) +')';
console.log(document.body.style.background);


var wrap     = document.getElementById('app'),
    colorBox = document.createElement('div'),
    block    = document.createElement('div'),
    btn      = document.createElement('button');

block.setAttribute('class', 'center-block');
colorBox.setAttribute('class', 'color-box');
colorBox.innerText = 'Нажми на меня!!!';
btn.innerText = 'change color now';

block.appendChild(colorBox);
block.appendChild(btn); 

wrap.appendChild(block);

btn.onclick = function () {
  

  document.body.style.background  = 'rgba('+ getRandomIntInclusive(0, 255) +', '+ getRandomIntInclusive(0, 255) +', '+ getRandomIntInclusive(0, 255) +')';
  console.log(document.body.style.background);

};

colorBox.onclick = function() {
    var hex = '#';

  for ( var i = 0; i < 3; i ++ ) {
    var color = getRandomIntInclusive(0, 255).toString(16);

    if (color.length < 2) {
      color = 'f' + color;
    }

    hex += color; 

  }

  colorBox.style.background = hex;
  console.log(hex);

}



