import classes from './classes/';
console.log(classes);

let { Ostrich, Dove } = classes;

let ostrich = new Ostrich();

ostrich.eat();
ostrich.fly();
ostrich.swim();

let dove = new Dove();

dove.eat();
dove.hunt();
dove.sing();


// export default { Ostrich, Dove };