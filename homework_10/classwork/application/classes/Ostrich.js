import eat from './../modules/eat.js';
import fly from './../modules/fly.js';
import swim from './../modules/swim.js';



function Ostrich (name) {
  
  	this.name = 'Ostrich';
    this.eat = eat;
    this.fly = fly;
    this.swim = swim;
  
}

export default Ostrich;