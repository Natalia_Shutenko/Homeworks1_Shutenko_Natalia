import eat from './../modules/eat.js';
import hunt from './../modules/hunt.js';
import sing from './../modules/sing.js';



function Dove (name) {

  	this.name = 'Dove';
    this.eat = eat;
    this.hunt = hunt;
    this.sing = sing;

}

export default Dove;