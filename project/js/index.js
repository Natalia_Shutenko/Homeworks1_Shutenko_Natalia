document.addEventListener('DOMContentLoaded', function(){
    let counter = 0,
        posts = [];


    class Post {
        constructor(id, author, text, src, date, answers, likes) {
            this.id = id;
            this.author = author;
            this.text = text;
            this.src = src;
            this.date = date;
            this.answers = answers;
            this.likes = likes;

            this.showMessage = this.showMessage.bind(this);
            this.answerMessage = this.answerMessage.bind(this);
            this.sendAnswer = this.sendAnswer.bind(this);
            this.addLike = this.addLike.bind(this);

            posts.push(this);

        }

        showMessage ( answers) {
            console.log(this.answers);
            return this;
        };

        answerMessage () {
            this.answer = true;
            renderPostList(posts);

        };

        sendAnswer (answer) {
            this.answers.push(answer);
            renderPostList(posts);
        };


        addLike (element) {
            this.likes++;
            element.target.innerText = 'Likes ' + this.likes;
            return this;
        }

        render () {
            let post = document.createElement('div');
                post.className = 'news-list__item news-post';

            post.innerHTML += `
                <div data-id="${this.id}">
                    <div class="news-post__inner">
                        <p class="news-post__author">Author: <span>${this.author}<span></p>
                        <p class="news-post__date">Date: <span>${this.date}</span></p>
                        <p class="news-post__text"> 
                            ${this.text}
                        </p>
                        <img class="news-post__img" src="${this.src}" alt="">
                    </div>
                </div> 
            `;


            let likeBtn = document.createElement('button');
                likeBtn.className = 'btn-like btn';
                likeBtn.innerText = 'Likes ' + this.likes;
                likeBtn.addEventListener('click', this.addLike);

                post.appendChild(likeBtn);   

            let answerBtn = document.createElement('button');
                answerBtn.className = 'btn-write-comment btn';
                answerBtn.id = 'answer-' + this.id;
                answerBtn.innerText = 'Write Comment'; 
                post.appendChild(answerBtn);

                answerBtn.addEventListener('click', e => {
                    this.answerMessage();

                    let commentForm = document.getElementById(this.id);
                        commentForm.classList.toggle('hidden');
                });
                


            let showBtn = document.createElement('button');
                showBtn.className = 'btn-show-comment btn';
                showBtn.id = 'show-' + this.id;
                showBtn.innerText = 'Show Comments ' + this.answers.length;

                showBtn.addEventListener('click', e => {
                    this.showMessage;
                    let commentBox = document.getElementById('box-' + this.id);
                        commentBox.classList.toggle('hidden');
                });


                post.appendChild(showBtn);


            if ( this.answer ) {

                let commentDiv = document.createElement('div'),
                    commentForm = document.createElement('form'),
                    commentAuthor = document.createElement('input'),
                    commentText = document.createElement('textarea'),
                    commentSend = document.createElement('button');

                    commentDiv.classList.add('constructor__wrap');
                    commentDiv.id = this.id;
                    commentDiv.classList.add('hidden');
                    commentAuthor.classList.add('constructor__wrap-input');
                    commentAuthor.setAttribute('placeholder', 'Author');
                    commentText.classList.add('constructor__wrap-input');
                    commentText.setAttribute('placeholder', 'Your comment');
                    commentSend.classList.add('btn');
                    commentSend.innerText = 'Send';

                    commentForm.appendChild(commentAuthor);
                    commentForm.appendChild(commentText);
                    commentForm.appendChild(commentSend);
                    commentDiv.appendChild(commentForm);

                    post.appendChild(commentDiv);

                    commentSend.addEventListener('click', e => {
                        e.preventDefault();

                        let id = ++counter,
                            author = commentAuthor.value,
                            text = commentText.value,
                            src = '',
                            date = new Date().toLocaleDateString() + ' ' + new Date().toLocaleTimeString(),
                            answers = [],
                            likes = 0,
                            parentId = this.id;


                        let answer = new Answer(id, author, text, src, date, answers, likes, parentId);

                        this.sendAnswer(answer);
                    });
            }


            if ( this.answers ) {
                let commentsBox = document.createElement('div');
                    commentsBox.className = 'constructor__comment hidden';
                    commentsBox.id = 'box-' + this.id;
                let content = '';

                    this.answers.map(answer => {
                        content += `
                            <div class="output_comments news-post">
                                <p class="news-post__author">Author: <span>${answer.author}</span></p>
                                <p class="news-post__date">Date: <span>${answer.date}</span></p>
                                <p class="news-post__text">${answer.text}</p>
                            </div> 
                        `;
                    });
                commentsBox.innerHTML = content;
                post.appendChild(commentsBox);

            }

            return post;
        }
    }

    class Answer extends Post {
        constructor (id, author, text, src, date, answers, likes, parentId) {
            super(id, author, text, src, date, answers, likes);
            this.parentId = parentId;
        }
    }

    document.getElementById('sendPost').addEventListener('click', (e) => {
        e.preventDefault();
        let id = ++counter,
            author = document.getElementById('author').value,
            text = document.getElementById('text').value,
            src = document.getElementById('img').value,
            date = new Date().toLocaleDateString() + ' ' + new Date().toLocaleTimeString(),
            answers = [],
            likes = 0;

        let post = new Post(id, author, text, src, date, answers, likes);

        renderPostList(posts);
        document.getElementById('form-constructor').reset();
        document.getElementById('preview').setAttribute('src', '');

    });

    

    const renderPostList = post => {
        let postList = document.getElementById('output_posts');
            postList.innerHTML = '';

        post.map(message => {
            postList.appendChild(message.render());
        });
    } 

    const getPreviewSrc = () => {
        let imageLink = document.getElementById('img'),
            preview = document.getElementById('preview');

        imageLink.addEventListener('change', () => {
            preview.src = imageLink.value;

            console.log( imageLink.value);
        });
    }

    getPreviewSrc ();

});
