
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Array

*/

let form1       = document.getElementById('formUser'),
	form2 		= document.getElementById('getJSON'),
	firstInput = document.getElementById('firstname'),
	lastInput  = document.getElementById('lastName'),
	ageInput   = document.getElementById('age'),
	parser 	   = document.getElementById('parser');

	form1.addEventListener('submit', (e) => {

		let name1 = firstInput.value,
		    name2 = lastInput.value,
		    age = ageInput.value,
		    user = {};

		e.preventDefault();

		user = {
			firstName: name1,
			lastName: name2,
			age: age
		}

		let jsonObj = JSON.stringify(user);

		console.log(jsonObj );

	});

	form2.addEventListener('submit', (e) => {
		e.preventDefault();
		let myJSON = parser.value;

			
		myObject = JSON.parse(myJSON);

		console.log(myObject);
	});




