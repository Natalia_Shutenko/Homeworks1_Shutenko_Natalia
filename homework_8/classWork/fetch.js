/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/
window.addEventListener('DOMContentLoaded', () => {
   let user = 'http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2';
   let friends = 'http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2';

    const convJson = result => result.json();

    const getUser = usersData => usersData[Math.floor(Math.random() * usersData.length)];

    const addFriends = user => {

      const userAddFriends = friendsList => (
          {
              'name' : user.name,
              'friends' : friendsList[0].friends
          }
      );

      return fetch(friends).then(convJson).then(userAddFriends);
    };

    const renderUser = user => {
        let renderedUser = `
            <h1>${user.name}</h3>
            Friends:
            ${user.friends.map(friend => `<div>${friend.name}</div>`)}
        `;

        let div = document.createElement('div');
            div.innerHTML = renderedUser;

            document.body.appendChild(div);
    };

   fetch(user).then(convJson).then(getUser).then(addFriends).then(renderUser);
});
