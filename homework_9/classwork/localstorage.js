
// console.log('test');


/*
  Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.
*/

window.addEventListener('DOMContentLoaded', e => {
 let btn = document.getElementById('btn'),
     body = document.body,
     color = localStorage.getItem('background');


    btn.addEventListener('click', e => {
        let back = getRandomColor (),
            color = localStorage.getItem('background');

        localStorage.setItem('background', back);
        body.style.background = color;

    });
    body.style.background = color;

    
    function getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        indexColor = Math.floor(Math.random() * (max - min + 1)) + min;
        return indexColor;
    }

    function getRandomColor () {
       return 'rgba('+ getRandomIntInclusive(0, 255) +', '+ getRandomIntInclusive(0, 255) +', '+ getRandomIntInclusive(0, 255) +')'
    }

});

/*

  Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678
*/


window.addEventListener('DOMContentLoaded', () => {

    document.getElementById('login').addEventListener('submit', getLogin);
    document.getElementById('logout').addEventListener('click', getLogout);

    login();

    function getLogin(e) {
        e.preventDefault();
        let user = {
            name : this.name.value,
            password : this.password.value,
        };
        localStorage.setItem('user', JSON.stringify(user));
        login();
    }


    function getLogout(e) {
        localStorage.removeItem('user');
        login();
    }


    function login() {
        if ( user = validation() ) {
            document.getElementById('login').classList.add('hidden');
            document.getElementById('welcome').classList.remove('hidden');
            document.querySelector('#welcome > h3').innerHTML = `Hello ${user.name}`;
        } else {
            document.getElementById('login').classList.remove('hidden');
            document.getElementById('welcome').classList.add('hidden');
        }
    }


    function validation() {
        let user = localStorage.getItem('user');
        if ( user != null ) {
            user = JSON.parse(user);
            if ( 'admin@example.com' === user.name && '12345678' === user.password ) {
                return user;
            }
        }
        return false;
    }


   
});
